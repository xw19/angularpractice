import { Recipe } from './recipe.model'
import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {
    recipeSelected = new EventEmitter<Recipe>();

    private recipes: Recipe[] = [
        new Recipe(
            'Hamburger',
            'Hamburger Recipe', 
            'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/RedDot_Burger.jpg/800px-RedDot_Burger.jpg',
            [
                new Ingredient('Bread', 2),
                new Ingredient('Patty', 1),
                new Ingredient('Lettuce', 2),

            ]
        ),
        new Recipe(
            'Schnitzel',
            'Schnitzel Recipe', 
            'https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Breitenlesau_Krug_Br%C3%A4u_Schnitzel.JPG/800px-Breitenlesau_Krug_Br%C3%A4u_Schnitzel.JPG',
            [
                new Ingredient('Meat', 1),
                new Ingredient('French fries', 20),

            ]
        ),
                
    ];


    constructor(private shoppingListService: ShoppingListService) {}

    getRecipe(index: number) {
        return this.recipes[index]
    }

    getRecipes() {
        return this.recipes.slice();
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.shoppingListService.pushIngredients(ingredients);
    }
}